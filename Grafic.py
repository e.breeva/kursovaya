import matplotlib.pyplot as plt
import csv

def save(param,napr, benchmark_type):
    try:
        param=str(param).replace('.',',')
    except:
        param=str(param)
    if benchmark_type == 'time':
        filename='dlyagraftime.csv'
    elif benchmark_type == 'memory':
        filename='dlyagrafrazmer.csv'        
    try:
        print(filename)
        file=open(filename,'x')
    except:
        file=open(filename,'a',newline="")
    file_writer=csv.writer(file,delimiter=";",lineterminator="\r",)    
    file_writer.writerow([param,napr])
    file.close()
    print("file save")
def loadgraf(benchmark_type):
    if benchmark_type == 'time':
        filename='dlyagraftime.csv'
        plt.ylabel('время выполнения, с')
    elif benchmark_type == 'memory':
        filename='dlyagrafrazmer.csv'
        plt.ylabel('объем занимаемой памяти, кб ')
    gry1=[]
    grx=[]
    file=open(filename)
    file_reader=csv.reader(file,delimiter=";")
    for row in file_reader:
        try:
            gry1.append(float(row[0].replace(',','.')))
        except:
            gry1.append(row[0])
        grx.append(int(row[1]))
    file.close()
    plt.xlabel('Количество направлений, шт')
    plt.grid()
    plt.plot(grx,gry1)
    plt.show()
