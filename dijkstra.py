import heapq as hq
import math
import time
import random
import tracemalloc


def dijkstra(G, s, benckmark_type):
    def _deikstra_(G, s):
        n = len(G)  # количество вершин
        visited = [False]*n  # посещена вершина или нет. Начальное заполнение
        weights = [math.inf]*n  # Начальное заполнение длины пути
        path = [None]*n
        queue = []
        weights[s] = 0  # путь до опорной вершины
        hq.heappush(queue, (0, s))  # минимальный элемент в кучу (вес,вершина)
        while len(queue) > 0:
            g, u = hq.heappop(queue)  # забираем из кучи вес и вершину
            visited[u] = True  # отмечаеим посещенную вершинуу
            for v, w in G[u]:  # для вершины и веса в графе
                if not visited[int(v)]:  # если вершина не посещена
                    f = g + float(w)  # вес ребра+длина пройденного пути
                    if f < weights[int(v)]:  # если новый путь <
                        # длина пути до вершины=новая длина пути
                        weights[int(v)] = f
                        path[int(v)] = u  # в путь добавляется вершина
                        # в кучу добавляется новая длина пути, вершина)
                        hq.heappush(queue, (f, int(v)))
        print(time.time())
        return path, weights

    # benchmarks: time, memory
    if benckmark_type == 'time':
        start_time = time.time()
        print(start_time)
        path,weights = _deikstra_(G, s)
        end_time = time.time()-start_time  # время выполнения
        
        return path,weights, end_time
    elif benckmark_type == 'memory':
        tracemalloc.start()
        path,weights = _deikstra_(G, s)
        _, first_peak = tracemalloc.get_traced_memory()  # подсчет памяти
        return path,weights, first_peak
    elif benckmark_type == 'none':
        return _deikstra_(G, s)
