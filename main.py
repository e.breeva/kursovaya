import dijkstra 
import zap
import Grafic
import networkx as nx##установить
import matplotlib.pyplot as plt
#вершина,вес ребра к этой вершине

mode = input('Выберите режим работы программы: 1 - граф из примера, 2 - ручной ввод: ')

if mode == '1':
    G = [[(1,2),(7,15)],#a-0 (3,3) (4,4)
    [(0,2),(2,1),(3,5)],#b-1 (6,5)
    [(1,1),(3,3),(5,2),(6,1)],#c-2
    [(1,5),(2,3),(4,6),(5,4)],#d-3 (0,3)
    [(3,6),(5,7),(8,2)],#e-4 (0,4)
    [(2,2),(6,1),(4,7),(7,3)],#f-5
    [(2,1),(5,1)],#g-6 (8,2) (1,5)
    [(0,15),(5,3),(8,12)],#h-7
    [(4,2),(7,12)]]#i-8 (6,2)
    # napr = ???
    # graph =  ???
    graph=nx.Graph()
    count = 0
    napr=0
    sp=[]
    li=[]
    z=0
    while z<int(len(G)-1):
        graph.add_node(z)
        z+=1
    for i in G:    
        for j in i:
            sp.append(count)
            napr+=1
            for z in j:
                sp.append(z)
            li.append(tuple(sp))
            sp=[]
        count+=1
    print(li)
    graph.add_weighted_edges_from(set(li))
elif mode == '2':
    G, napr, graph = zap.zapoln()

print("список смежности")
print(G)

# which benchmark????
benchmark_type = input('Какой бенчмарк? 1 - время, 2 - память ')
print(benchmark_type)
if benchmark_type == '1':
    path, weight, tim = dijkstra.dijkstra(G, int(input("Введите стартовую вершину = ")), 'time')
    Grafic.save(tim, napr, 'time')
    Grafic.loadgraf('time')
elif benchmark_type == '2':
    path, weight, kb = dijkstra.dijkstra(G, int(input("Введите стартовую вершину = ")), 'memory')
    Grafic.save(kb, napr, 'memory')
    Grafic.loadgraf('memory')
else: 
    path, weight = dijkstra.dijkstra(G, int(input("Введите стартовую вершину = ")), 'none')
print("путь")
print(path)
print("длина кратчайшего пути до всех вершин")
print(weight)

nx.draw_circular(graph, node_color='red', node_size=1000, with_labels=True)
plt.show()
